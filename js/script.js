$(function()
{
	//---------------
	//* App options Model
	var ApplicationParams = Backbone.Model.extend(
	{
		imagesPathPrefix: "data/img/"
	});
	
	var AppParams = new ApplicationParams();
	
	//---------------
	//* Category Model
	var Category = Backbone.Model.extend(
	{
		defaults:
		{
			cid: 0,
			title: "Название по-русски",
			titleEn: "title",
			subCategories: [],
			parentCategory: 0
		}
	});
	
	var CategoryList = Backbone.Collection.extend({
		model: Category,
		initialize: function()
		{
			this.add({cid: 10, title: "Салаты", parentCategory: 0});
			this.add(
			{
				tid: 8,
				title: "Первые блюда",
				parentCategory: 0
			});
		}
	});
	
	/*
	var str='';
$('li[rel*=tid]').each(function()
{
	var tid = $(this).attr('rel').split('-')[1];
	str += '{\n'+
		'\ttid: '+tid+',\n'+
		'\ttitle: "'+''+'"\n'+
		'},\n';
});
console.log(str)
	*/
	
	var ctgrlst = new CategoryList();
	console.log(ctgrlst);
	
	//---------------
	//* Recipe Model
	var Recipe = Backbone.Model.extend(
	{
		initialize: function()
		{
           console.log("Oh hey! ");
		},
		defaults:
		{
			nid: 0,
			title: "",
			calories: 0,
			cooktime: 0,
			portions: 0,
			filepath: AppParams.imagesPathPrefix + "empty.jpg",
			tags: []
		}
	});
	
	//---------------
	//* Recipes Collection
	var RecipeList = Backbone.Collection.extend(
	{
		model: Recipe,
		//* filling collection from existed recipes-object
		fillCollection: function(recipesObj)
		{
			for(var i in recipesObj)
			{
				this.add(
				{
					nid: i,
					title: recipesObj[i]['title'],
					calories: recipesObj[i]['calories'],
					cooktime: recipesObj[i]['cooktime'],
					portions: recipesObj[i]['portions'],
					filepath: AppParams.imagesPathPrefix + ((recipesObj[i]['filepath']===null)?"empty.jpg":recipesObj[i]['filepath'].split('/')[2])
				});
			}
		}
	});

	//---------------
	//* Slide Model
	var Slide = Backbone.Model.extend(
	{
		defaults:
		{
			cid: 0,
			text: "Возьмите чистую кастрюлю",
			img: 'empty.jpg',
			slidePosition: 0
		}
	});
	
	var rcplst = new RecipeList();
	rcplst.fillCollection(tvoirecepti_recepts);
	console.log(rcplst);
	
	//---------------
	var ApplicationViews = { };
	
});