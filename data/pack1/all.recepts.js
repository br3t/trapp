var tvoirecepti_recepts = {
    "263": {
        "title": "Баклажаны фаршированные с шампиньонами",
        "calories": "154",
        "cooktime": "40",
        "portions": "6",
        "filepath": "files/recept/recept-baklazhany-farshirovannye-s-shampinonami.jpg",
        "comments": "0"
    },
    "264": {
        "title": "Запеканка картофельная с грибами",
        "calories": "302",
        "cooktime": "130",
        "portions": "8",
        "filepath": "files/recept/recept-zapekanka-kartofelnaya-s-gribami.jpg",
        "comments": "0"
    },
    "266": {
        "title": "Оладьи из кабачков",
        "calories": "74",
        "cooktime": "40",
        "portions": "8",
        "filepath": "files/recept/recept-oladi-iz-kabachkov.jpg",
        "comments": "0"
    },
    "270": {
        "title": "Сырники из творога",
        "calories": "198",
        "cooktime": "40",
        "portions": "8",
        "filepath": "files/recept/recept-syrniki-tvorozhnye.jpg",
        "comments": "0"
    },
    "280": {
        "title": "Рыба на гриле",
        "calories": "338",
        "cooktime": "20",
        "portions": "6",
        "filepath": "files/recept/recept-ryba-na-grile.jpg",
        "comments": "0"
    },
    "288": {
        "title": "Рассольник",
        "calories": "628",
        "cooktime": "100",
        "portions": "8",
        "filepath": "files/recept/recept-rassolnik.jpg",
        "comments": "0"
    },
    "290": {
        "title": "Окрошка",
        "calories": "628",
        "cooktime": "40",
        "portions": "4",
        "filepath": "files/recept/recept-okroshka.jpg",
        "comments": "1"
    },
    "302": {
        "title": "Утка с яблоками",
        "calories": "1326",
        "cooktime": "160",
        "portions": "6",
        "filepath": "files/recept/recept-utka-s-yablokami.jpg",
        "comments": "0"
    },
    "303": {
        "title": "Рыба в кляре",
        "calories": "235",
        "cooktime": "120",
        "portions": "8",
        "filepath": "files/recept/recept-ryba-v-klyare.jpg",
        "comments": "0"
    },
    "306": {
        "title": "Суп с галушками",
        "calories": "550",
        "cooktime": "60",
        "portions": "6",
        "filepath": "files/recept/recept-sup-s-galushkami.jpg",
        "comments": "0"
    },
    "315": {
        "title": "Харчо",
        "calories": "468",
        "cooktime": "130",
        "portions": "8",
        "filepath": "files/recept/recept-kharcho.jpg",
        "comments": "0"
    },
    "328": {
        "title": "Мясо по-французски",
        "calories": "1202",
        "cooktime": "90",
        "portions": "6",
        "filepath": "files/recept/recept-myaso-po-frantsuzski.jpg",
        "comments": "0"
    },
    "329": {
        "title": "Селедка под шубой",
        "calories": "717",
        "cooktime": "60",
        "portions": "6",
        "filepath": "files/recept/recept-seledka-pod-shuboi.jpg",
        "comments": "0"
    }
};